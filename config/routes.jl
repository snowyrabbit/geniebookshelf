using Genie.Router
using BooksController


route("/") do
  serve_static_file("/welcome.html")
end

route("/hello") do
  "Hello, Genie!"
end

route("/books", BooksController.show_books, named = :show_books)

route("/books/register", BooksController.register, named = :register_book)

route("/books/create", BooksController.create, method = POST, named = :create_book)

# 本の編集画面
route("/books/:id::Int/edit", BooksController.edit, named = :edit_book)

# 本の情報のアップデート処理
route("/books/:id::Int/update", BooksController.update, method = POST, named = :update_book)

# 本の削除処理
route("/books/:id::Int/unregister", BooksController.unregister, method = POST, named = :unregister_book)
