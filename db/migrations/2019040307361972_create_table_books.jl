module CreateTableBooks

import SearchLight.Migrations: create_table, column, primary_key, add_index, drop_table

function up()
  create_table(:books) do
    [
    # Bookモデルに合うようにカラムを書き直す
      primary_key()
      column(:title, :string)
      column(:publication_date, :string)
      column(:cover, :string)
    ]
  end

  # インデックスを追加
  add_index(:books, :title)
  add_index(:books, :publication_date)
  add_index(:books, :cover)
end

function down()
  drop_table(:books)
end


end
