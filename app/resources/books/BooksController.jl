module BooksController


using Genie.Requests, Genie.Renderer, SearchLight, Books, Genie.Router


"""
本の一覧画面を表示する
"""
function show_books()
    html!(:books, :show_books, books = SearchLight.all(Book))
end


"""
本の登録画面を表示する
"""
function register()
    html!(:books, :register)
end

"""
本の登録処理を行う
"""
function create()
    # アップロードした画像データをbook_coverディレクトリに保存して、パスを取得
      cover_path = if haskey(filespayload(), "book_cover")
      path = joinpath("img", "app", "book_covers", filespayload("book_cover").name)
      write(joinpath("public", path), IOBuffer(filespayload("book_cover").data))
      "/" * path  #  /img以下のパス
    # アップロード画像がない場合は空のパスを設定
    else
      ""
  end

  # フォームからデータを取得してDBに保存
  new_book = Book(title = @params(:book_title), publication_date = @params(:book_pub_date), cover = cover_path)
  save!(new_book)
  return redirect_to(:show_books)
end

"""
本の編集ページを表示する
"""
function edit()
    html!(:books, :edit, book = SearchLight.find_by(Book, :id, @params(:id)) |> first)
end


"""
本の登録情報を変更する
"""
function update()
    #idから現時点の本の情報を取得する
    updated_book = SearchLight.find_by(Book, :id, @params(:id)) |> first

    # タイトルに変更があるとき
    if @params(:book_title) != ""
        updated_book.title = @params(:book_title)
    end

    # 出版日に変更があるとき
    if @params(:book_pub_date) != ""
        updated_book.publication_date = @params(:book_pub_date)
    end

    # カバーページに変更があるとき
    if haskey(filespayload(), "book_cover")
        path = joinpath("img", "app", "book_covers", filespayload("book_cover").name)
        write(joinpath("public", path), IOBuffer(filespayload("book_cover").data))
        updated_book.cover = "/" * path
    end

    # 変更を保存して、一覧ページへリダイレクト
    save!(updated_book)
    return redirect_to(:show_books)
end


"""
本を削除する
"""
function unregister()
    # idから本の情報を取得して削除
    unregistered_book = SearchLight.find_by(Book, :id, @params(:id)) |> first
    delete(unregistered_book)

    # 一覧ページへリダイレクト
    return redirect_to(:show_books)
end


end