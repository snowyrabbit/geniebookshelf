module Books


using SearchLight, Nullables, SearchLight.Validation, BooksValidator

export Book


mutable struct Book <: AbstractModel
  ### INTERNALS
  _table_name::String
  _id::String
  _serializable::Vector{Symbol}

  ### FIELDS
  id::DbId
  title::String
  publication_date::String
  cover::String

  ### constructor
  Book(;
    ### FIELDS
    id = DbId(),
    title = "",
    publication_date = "",
    cover = ""
  ) = new("books", "id", Symbol[],
          id, title, publication_date, cover
          )
end


function seed(book_list::Array)
  for book in book_list
    Book(title = book[1], publication_date = book[2], cover = book[3]) |> SearchLight.save!
  end
end


end
